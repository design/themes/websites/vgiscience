# Jekyll Theme Gem for Websites

**If you simply want to create a website within the VGIscience domain using this theme, just go ahead and fork [the Website Template](https://gitlab.vgiscience.de/templates/website) and follow the instructions in that project's `README.md`. It already utilizes this theme gem.**

The following is only of interest for the integration in remote Jekyll instances.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "vgiscience-website-theme", :git => 'https://gitlab.vgiscience.de/design/themes/jekyll/vgiscience-website-theme.git'
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: vgiscience-website-theme
```

And then execute:

```shell
bundle update
```
