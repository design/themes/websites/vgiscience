# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "vgiscience-website-theme"
  spec.version       = "0.9.0"
  spec.authors       = ["Marc Löchner"]
  spec.email         = ["ml@vgiscience.org"]

  spec.summary       = "Default jekyll theme for VGIscience-related static sites"
  spec.homepage      = "https://gitlab.vgiscience.de/design/themes/websites/vgiscience"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", ">= 3.7", "< 5.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12"
end
